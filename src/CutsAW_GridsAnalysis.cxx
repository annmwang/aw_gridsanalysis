#include "CutsAW_GridsAnalysis.h"
#include "ConfigSvc.h"

CutsAW_GridsAnalysis::CutsAW_GridsAnalysis(EventBase* eventBase)
{
    m_event = eventBase;
}

CutsAW_GridsAnalysis::~CutsAW_GridsAnalysis()
{
}
bool CutsAW_GridsAnalysis::isGoodBufferStart(unsigned long triggerTimeStamp_s, unsigned long triggerTimeStamp_ns, std::vector<unsigned long> bufferStartTime_s, std::vector<unsigned long> bufferStartTime_ns){

  int preTriggerWindow_ns = 1500000;
  bool isGoodBufferStart = true;
  for(int b=0; b<45; b++){           // buffers 0-31 are TPC, buffers 32-36 are skin, buffers 37-44 are OD
    if((triggerTimeStamp_s - bufferStartTime_s[b])*1000000000 + (triggerTimeStamp_ns - bufferStartTime_ns[b]) < preTriggerWindow_ns){
      isGoodBufferStart = false;
    }
  }
  return isGoodBufferStart;
}

// Function that lists all of the common cuts for this Analysis
bool CutsAW_GridsAnalysis::AW_GridsAnalysisCutsOK()
{
    // List of common cuts for this analysis into one cut
    return true;
}
