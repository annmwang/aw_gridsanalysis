#include "AW_GridsAnalysis.h"
#include "Analysis.h"
#include "ConfigSvc.h"
#include "CutsBase.h"
#include "CutsAW_GridsAnalysis.h"
#include "EventBase.h"
#include "HistSvc.h"
#include "Logger.h"
#include "SkimSvc.h"
#include "SparseSvc.h"

// Choose 0 for November runs, 1 for Run 6621, 2 for Run 6622
int run_group = 2;

// Constructor
AW_GridsAnalysis::AW_GridsAnalysis()
    : Analysis()
{
    // Set up the expected event structure, include branches required for analysis.
    // List the branches required below, see full list here https://luxzeplin.gitlab.io/docs/softwaredocs/analysis/analysiswithrqs/rqlist.html
    // Load in the Single Scatters Branch
    m_event->IncludeBranch("eventHeader");
    m_event->IncludeBranch("pulsesTPC");
    m_event->IncludeBranch("eventTPC");
    m_event->Initialize();
    ////////

    // Setup logging
    logging::set_program_name("AW_GridsAnalysis Analysis");

    // Setup the analysis specific cuts.
    m_cutsAW_GridsAnalysis = new CutsAW_GridsAnalysis(m_event);

    // Setup config service so it can be used to get config variables
    m_conf = ConfigSvc::Instance();
}

// Destructor
AW_GridsAnalysis::~AW_GridsAnalysis()
{
    delete m_cutsAW_GridsAnalysis;
}

/////////////////////////////////////////////////////////////////
// Start of main analysis code block
//
// Initialize() -  Called once before the event loop.
void AW_GridsAnalysis::Initialize()
{
    INFO("Initializing AW_GridsAnalysis Analysis");
}

// Execute() - Called once per event.
void AW_GridsAnalysis::Execute()
{
  // Event info
  double firstTriggerTime;
  if (run_group == 0)
    firstTriggerTime = 1637989583.;
  else if (run_group == 1)
    firstTriggerTime = 1639453528.;
  else if (run_group == 2)
    firstTriggerTime = 1639474372.;

  //ER-only 1634511521
 double triggerEndTime;
  if (run_group == 0)
    triggerEndTime = 140040.;
  else if (run_group == 1)
    triggerEndTime = 20700.;
  else if (run_group == 2)
    triggerEndTime = 20400.;
  //ER-only 50400.
  double triggerTime      = (*m_event->m_eventHeader)->triggerTimeStamp_s+(*m_event->m_eventHeader)->triggerTimeStamp_ns*pow(10,-9)-firstTriggerTime;

  int nbins              = triggerEndTime/60.;
  int ihour              = floor(triggerTime/60./60.);
  int triggerType        = (*m_event->m_eventHeader)->triggerType;
  int runID              = (*m_event->m_eventHeader)->runID;


  m_hists->BookFillHist("evt_cutflow", 10, -0.5, 9.5, 0);
  // Require random trigger
  if (triggerType == 32){
    m_hists->BookFillHist("evt_cutflow", 10, -0.5, 9.5, 1);
    m_hists->BookFillHist("triggerTime_random",nbins, 0., triggerEndTime, float(triggerTime));
    m_hists->BookFillHist("runID_vs_triggerTime_random", nbins, 0., triggerEndTime, 20, 6440.5,6460.5, float(triggerTime), float(runID));

    // Loop through all pulses, categorize pulses
    int nPulses = (*m_event->m_tpcPulses)->nPulses;
    for (int i = 0; i < nPulses; i++){
      
      int AFT_5ns     = (*m_event->m_tpcPulses)->areaFractionTime5_ns[i];
      int AFT_95ns    = (*m_event->m_tpcPulses)->areaFractionTime95_ns[i];
      float pulseArea = (*m_event->m_tpcPulses)->pulseArea_phd[i];
      string classification = (*m_event->m_tpcPulses)->classification[i] ;
      float s2Xposition_cm  = (*m_event->m_tpcPulses)->s2Xposition_cm[i];
      float s2Yposition_cm  = (*m_event->m_tpcPulses)->s2Yposition_cm[i];
      int start_time = (*m_event->m_tpcPulses)->pulseStartTime_ns[i];
      int end_time   = (*m_event->m_tpcPulses)->pulseEndTime_ns[i];
      int nCoinc = (*m_event->m_tpcPulses)->coincidence[i];

      std::vector<float> chPulseArea_phd = (*m_event->m_tpcPulses)->chPulseArea_phd[i];
      int nMultAbove100 = 0;
      for (int ich = 0; ich < nCoinc; ich++){
        if (chPulseArea_phd[ich] > 100)
          nMultAbove100++;
      }
      
      m_hists->BookFillHist("pulse_cutflow", 10, -0.5, 9.5, 0);

      // Cut out pulses which are before the trigger
      // Mitigates bias from any PU from S2 trigger
      if (start_time < 0.)
        continue;
      
      m_hists->BookFillHist("pulse_cutflow", 10, -0.5, 9.5, 1);
      m_hists->BookFillHist("log10pulseArea_tpc_vs_log10AFT95mAFT5",500,0,6,500,-2,10, log10(AFT_95ns-AFT_5ns), log10(pulseArea));
      m_hists->BookFillHist("Y_vs_X",500,-100,100,500,-100,100, s2Xposition_cm, s2Yposition_cm); 
      
      if (classification == "SE"){
        m_hists->BookFillHist("log10pulseArea_tpc_vs_log10AFT95mAFT5_SEonly",500,0,6,500,-2,10, log10(AFT_95ns-AFT_5ns), log10(pulseArea));
        m_hists->BookFillHist("pulseArea_tpc_vs_AFT95mAFT5_SEonly",500,560.,10000,280,20,160, AFT_95ns-AFT_5ns, pulseArea);
        m_hists->BookFillHist("SE_Y_vs_X",500,-100,100,500,-100,100, s2Xposition_cm, s2Yposition_cm); // how is this defined?
        m_hists->BookFillHist("SE_triggerTime",nbins, 0., triggerEndTime, float(triggerTime));
        m_hists->BookFillHist("SE_nCoinc",500, -0.5, 499.5, nCoinc);
        m_hists->BookFillHist("SE_nMultAbove100",500, -0.5, 499.5, nMultAbove100);
        m_hists->BookFillHist("SE_nCoinc_vs_pulseArea",500,0,5000,500, -0.5, 499.5, pulseArea,nCoinc);
      }
      if (classification == "SPE"){
        m_hists->BookFillHist("log10pulseArea_tpc_vs_log10AFT95mAFT5_SPEonly",500,0,6,500,-2,10, log10(AFT_95ns-AFT_5ns), log10(pulseArea));
        m_hists->BookFillHist("SPE_nCoinc",500, -0.5, 499.5, nCoinc);
        m_hists->BookFillHist("SPE_nMultAbove100",500, -0.5, 499.5, nMultAbove100);
        m_hists->BookFillHist("SPE_nCoinc_vs_pulseArea",500,0,5000,500, -0.5, 499.5, pulseArea,nCoinc);
      }
      if (classification == "S1"){
        m_hists->BookFillHist("log10pulseArea_tpc_vs_log10AFT95mAFT5_S1only",500,0,6,500,-2,10, log10(AFT_95ns-AFT_5ns), log10(pulseArea));
        m_hists->BookFillHist("pulseArea_tpc_vs_AFT95mAFT5_S1only",500,560.,10000,280,20,160, AFT_95ns-AFT_5ns, pulseArea);
        m_hists->BookFillHist("S1_nCoinc",500, -0.5, 499.5, nCoinc);
        m_hists->BookFillHist("S1_nMultAbove100",500, -0.5, 499.5, nMultAbove100);
        m_hists->BookFillHist("S1_nCoinc_vs_pulseArea",500,0,5000,500, -0.5, 499.5, pulseArea,nCoinc);
      }
      if (classification == "S2"){
        m_hists->BookFillHist("log10pulseArea_tpc_vs_log10AFT95mAFT5_S2only",500,0,6,500,-2,10, log10(AFT_95ns-AFT_5ns), log10(pulseArea));
        m_hists->BookFillHist("pulseArea_tpc_vs_AFT95mAFT5_S2only",500,560.,10000,1720,140,1000, AFT_95ns-AFT_5ns, pulseArea);
        m_hists->BookFillHist("S2_triggerTime",nbins, 0., triggerEndTime, float(triggerTime));
        m_hists->BookFillHist("S2_nCoinc",500, -0.5, 499.5, nCoinc);
        m_hists->BookFillHist("S2_nMultAbove100",500, -0.5, 499.5, nMultAbove100);
        m_hists->BookFillHist("S2_nCoinc_vs_pulseArea",500,0,5000,500, -0.5, 499.5, pulseArea,nCoinc);
      }
      if (classification == "MPE"){
        m_hists->BookFillHist("log10pulseArea_tpc_vs_log10AFT95mAFT5_MPEonly",500,0,6,500,-2,10, log10(AFT_95ns-AFT_5ns), log10(pulseArea));
        m_hists->BookFillHist("MPE_nCoinc",500, -0.5, 499.5, nCoinc);
        m_hists->BookFillHist("MPE_nMultAbove100",500, -0.5, 499.5, nMultAbove100);
        m_hists->BookFillHist("MPE_nCoinc_vs_pulseArea",500,0,5000,500, -0.5, 499.5, pulseArea,nCoinc);
      }
      if (classification == "Other"){
        m_hists->BookFillHist("log10pulseArea_tpc_vs_log10AFT95mAFT5_Otheronly",500,0,6,500,-2,10, log10(AFT_95ns-AFT_5ns), log10(pulseArea));
        m_hists->BookFillHist("Other_nCoinc",500, -0.5, 499.5, nCoinc);
        m_hists->BookFillHist("Other_nMultAbove100",500, -0.5, 499.5, nMultAbove100);
        m_hists->BookFillHist("Other_nCoinc_vs_pulseArea",500,0,5000,500, -0.5, 499.5, pulseArea,nCoinc);
      }
      if (classification == "SE" || classification == "S2"){
        m_hists->BookFillHist("pulse_cutflow", 10, -0.5, 9.5, 2);
        m_hists->BookFillHist("pulseArea_SES2",500,0.,5000.,pulseArea);
        m_hists->BookFillHist("log10pulseArea_tpc_vs_log10AFT95mAFT5_SES2",500,0,6,500,-2,10, log10(AFT_95ns-AFT_5ns), log10(pulseArea));
        m_hists->BookFillHist("pulseArea_tpc_vs_AFT95mAFT5_SES2",500,560.,10000,2000,0,1000, AFT_95ns-AFT_5ns, pulseArea);
        m_hists->BookFillHist("SES2_Y_vs_X",500,-100,100,500,-100,100, s2Xposition_cm, s2Yposition_cm); 
        
        m_hists->BookFillHist("SES2_triggerTime",nbins, 0., triggerEndTime, float(triggerTime));
        
        m_hists->BookFillHist(Form("SES2_Y_vs_X_ihour%d",ihour),500,-100,100,500,-100,100, s2Xposition_cm, s2Yposition_cm); 
        
        // SR-1
        // Emitter 0 small circle
        
        double ctr0_x =  -65; 
        double ctr0_y = 1; 
        if (run_group == 0){
          ctr0_x = -65;
          ctr0_y = 9;
        }
        if (sqrt(pow(s2Xposition_cm-ctr0_x,2)+pow(s2Yposition_cm-ctr0_y,2)) < 10){
          m_hists->BookFillHist("SES2_Y_vs_X_emitter0_circle",500,-100,100,500,-100,100, s2Xposition_cm, s2Yposition_cm);
          m_hists->BookFillHist("SES2_triggerTime_emitter0_circle",nbins, 0., triggerEndTime, float(triggerTime));
          m_hists->BookFillHist("pulseArea_SES2_emitter0_circle",2000,0.,1000.,pulseArea);
          m_hists->BookFillHist("pulseArea_tpc_vs_AFT95mAFT5_SES2_emitter0",500,560.,10000,2000,0,1000, AFT_95ns-AFT_5ns, pulseArea);
        }
        
        // Emitter 1
        double ctr1_x = 68; 
        double ctr1_y = -2; 
        if (run_group == 0){
          ctr1_x = -34;
          ctr1_y = 44.5;
        }
        if (sqrt(pow(s2Xposition_cm-ctr1_x,2)+pow(s2Yposition_cm-ctr1_y,2)) < 10){
          m_hists->BookFillHist("SES2_Y_vs_X_emitter1_circle",500,-100,100,500,-100,100, s2Xposition_cm, s2Yposition_cm);
          m_hists->BookFillHist("SES2_triggerTime_emitter1_circle",nbins, 0., triggerEndTime, float(triggerTime));
          m_hists->BookFillHist("pulseArea_SES2_emitter1_circle",2000,0.,1000.,pulseArea);
          m_hists->BookFillHist("pulseArea_tpc_vs_AFT95mAFT5_SES2_emitter1",500,560.,10000,2000,0,1000, AFT_95ns-AFT_5ns, pulseArea);
        }

        // Emitter 2
        double ctr2_x = 14; 
        double ctr2_y = -62; 
        if (sqrt(pow(s2Xposition_cm-ctr2_x,2)+pow(s2Yposition_cm-ctr2_y,2)) < 10){
          m_hists->BookFillHist("SES2_Y_vs_X_emitter2_circle",500,-100,100,500,-100,100, s2Xposition_cm, s2Yposition_cm);
          m_hists->BookFillHist("SES2_triggerTime_emitter2_circle",nbins, 0., triggerEndTime, float(triggerTime));
          m_hists->BookFillHist("pulseArea_SES2_emitter2_circle",2000,0.,1000.,pulseArea);
          m_hists->BookFillHist("pulseArea_tpc_vs_AFT95mAFT5_SES2_emitter2",500,560.,10000,2000,0,1000, AFT_95ns-AFT_5ns, pulseArea);
        }

        // Background
        double bkg0_x = -20; 
        double bkg0_y = -61.85; 
        if (run_group == 0){
          bkg0_x = 65;
          bkg0_y = 9;
        }
        if (sqrt(pow(s2Xposition_cm-bkg0_x,2)+pow(s2Yposition_cm-bkg0_y,2)) < 10){
          m_hists->BookFillHist("SES2_Y_vs_X_bkg0",500,-100,100,500,-100,100, s2Xposition_cm, s2Yposition_cm);
          m_hists->BookFillHist("SES2_triggerTime_bkg0",nbins, 0., triggerEndTime, float(triggerTime));
          m_hists->BookFillHist("pulseArea_SES2_bkg0",2000,0.,1000.,pulseArea);
        }
        double bkg1_x = 2; 
        double bkg1_y = 68; 
        if (run_group == 0){
          bkg1_x = 34;
          bkg1_y = 44.5;
        }
        if (sqrt(pow(s2Xposition_cm-bkg1_x,2)+pow(s2Yposition_cm-bkg1_y,2)) < 10){
          m_hists->BookFillHist("SES2_Y_vs_X_bkg1",500,-100,100,500,-100,100, s2Xposition_cm, s2Yposition_cm);
          m_hists->BookFillHist("SES2_triggerTime_bkg1",nbins, 0., triggerEndTime, float(triggerTime));
          m_hists->BookFillHist("pulseArea_SES2_bkg1",2000,0.,1000.,pulseArea);
        }

        double bkg2_x = 49.4; //34;
        double bkg2_y = -40; //44.5;
        if (sqrt(pow(s2Xposition_cm-bkg2_x,2)+pow(s2Yposition_cm-bkg2_y,2)) < 10){
          m_hists->BookFillHist("SES2_Y_vs_X_bkg2",500,-100,100,500,-100,100, s2Xposition_cm, s2Yposition_cm);
          m_hists->BookFillHist("SES2_triggerTime_bkg2",nbins, 0., triggerEndTime, float(triggerTime));
          m_hists->BookFillHist("pulseArea_SES2_bkg2",2000,0.,1000.,pulseArea);
        }

          
      }
    }
  }
}

// Finalize() - Called once after event loop.
void AW_GridsAnalysis::Finalize()
{
    INFO("Finalizing AW_GridsAnalysis Analysis");
}
