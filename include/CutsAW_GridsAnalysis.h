#ifndef CutsAW_GridsAnalysis_H
#define CutsAW_GridsAnalysis_H

#include "EventBase.h"

class CutsAW_GridsAnalysis {

public:
    CutsAW_GridsAnalysis(EventBase* eventBase);
    ~CutsAW_GridsAnalysis();
    bool isGoodBufferStart(unsigned long triggerTimeStamp_s, unsigned long triggerTimeStamp_ns, std::vector<unsigned long> bufferStartTime_s, std::vector<unsigned long> bufferStartTime_ns);
    bool AW_GridsAnalysisCutsOK();

private:
    EventBase* m_event;
};

#endif
